# {{cookiecutter.project_name}}

Version {{cookiecutter.version}}

{{cookiecutter.project_short_description}}


## Project organization

The project structure distinguishes three kinds of folders:

- read-only (RO): not edited by either code or researcher
- human-writeable (HW): edited by the researcher only.
- project-generated (PG): folders generated when running the code; these folders can be deleted or emptied and will be completely reconstituted as the project is run.


```
.
├── .gitignore         <- Untracked files that git will ignore
├── CITATION.md        <- Contains information on how to cite the project
├── LICENSE.md         <- Copyright information
├── README.md
├── requirements.txt   <- Requirements for reproducing the analysis environment (PG)
├── bin                <- Compiled and external code, ignored by git (PG)
│   └── external       <- Any external source code, ignored by git (RO)
├── data               <- All project data, ignored by git
│   ├── processed      <- The final, canonical data sets for modeling. (PG)
│   ├── raw            <- The original, immutable data dump. (RO)
│   └── temp           <- Intermediate data that has been transformed. (PG)
├── docs               <- Documentation notebook for users (HW)
│   ├── manuscript     <- Manuscript source, e.g., LaTeX, Markdown, etc. (HW)
│   └── reports        <- Other project reports and notebooks (e.g. Jupyter, .Rmd) (HW)
├── results
│   ├── figures        <- Figures for the manuscript or reports (PG)
│   └── output         <- Other output for the manuscript or reports (PG)
└── src                <- Source code for this project (HW)

```

## Prerequisites

You need to have ... to generate the replicate the results of this study.

## License

This project is licensed under the terms of the [{{cookiecutter.license.split("(")[-1][:-1]}}](/LICENSE.md)

## Citation

Please [cite this project as described here](/CITATION.cff).
